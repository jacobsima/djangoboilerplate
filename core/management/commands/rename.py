import os
from django.core.management.base import BaseCommand


# https://docs.djangoproject.com/en/3.0/howto/custom-management-commands/
class Command(BaseCommand):
  help = 'Rename a django Project'

  def add_arguments(self,parser):
    parser.add_argument('new_project_name',type=str,help='The new Django project name')

    # '-p' : make if optional, not used here
    parser.add_argument('-p','--prefix')
  

  def handle(self,*args, **kwargs):
    new_project_name = kwargs['new_project_name']
    print(kwargs) 

    # bit of logic rename the project
    files_to_rename = ['configu/settings/base.py','configu/wsgi.py','manage.py']
    folder_to_rename = 'configu'

    # renaming files
    for f in files_to_rename:
      with open(f,'r') as file:
        filedata = file.read()
      
      filedata = filedata.replace('configu',new_project_name)

      with open(f,'w') as file:
        file.write(filedata)

    # renaming folder
    os.rename(folder_to_rename,new_project_name)


    # When you are using management commands and wish to provide console output, you should write to self.stdout and self.stderr,
    self.stdout.write(self.style.SUCCESS('Project has been renamed to %s'%new_project_name))
    

  