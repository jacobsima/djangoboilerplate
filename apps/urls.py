from django.urls import path,include
from .views import home

urlpatterns = [
    # http://127.0.0.1:8000/
    path('', home, name='home'),

    # http://127.0.0.1:8000/fbv/
    path('fbv/',include('apps.fbv.urls')),

    # http://127.0.0.1:8000/cbv/
    path('cbv/',include('apps.cbv.urls')),

    # http://127.0.0.1:8000/mixed/
    path('mixed/',include('apps.mixed.urls')),
]
