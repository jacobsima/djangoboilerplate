from django.urls import path
from .views import mixed_home_fbv

urlpatterns = [
    # http://127.0.0.1:8000/mixed/fbv/
    path('',mixed_home_fbv, name='mixed_home_fbv')
]
