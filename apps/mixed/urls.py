from django.urls import path, include
from .views import home_mixed

urlpatterns = [

    # http://127.0.0.1:8000/mixed/
    path('',home_mixed, name='home_mixed'),

    # http://127.0.0.1:8000/mixed/fbv/
    path('fbv/',include('apps.mixed.mixedFBV.urls')),

    # http://127.0.0.1:8000/mixed/cbv/
    path('cbv/',include('apps.mixed.mixedCBV.urls')),
]
