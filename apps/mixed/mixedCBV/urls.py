from django.urls import path
from .views import MixedCbvHome

urlpatterns = [
    # http://127.0.0.1:8000/mixed/cbv/
    path('',MixedCbvHome.as_view(), name='mixed_cbv_home')
]
