from django.shortcuts import render, HttpResponse
from django.views.generic import View

# Create your views here.
class MixedCbvHome(View):
  def get(self, request, *args, **kwargs):
    return HttpResponse('From Mixed CBV Home Page')

