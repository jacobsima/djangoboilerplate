from django.shortcuts import render, HttpResponse
from django.views.generic import View

# Create your views here.
class CbvHome(View):

  def get(self, request, *args, **kwrags):
    return HttpResponse('From Class Based View Home Base')
