from django.urls import path
from .views import CbvHome

urlpatterns = [
    # http://127.0.0.1:8000/cbv/
    path('', CbvHome.as_view(), name='home_cbv')
]

